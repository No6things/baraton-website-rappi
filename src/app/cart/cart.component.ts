import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MatSnackBar } from '@angular/material';

import { Product } from '../product/product.model';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'bar-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  providers: [ProductService]
})
export class CartComponent implements OnInit {

  constructor(
    public snackBar: MatSnackBar,
    private productService: ProductService
  ) { }

  products: Observable<Product[]>;

  ngOnInit() {
    this.products = this.productService.getCartProducts();
  }

  showBoughtSnackbar() {
    const message = 'Pedido tomado';
    this.snackBar.open(message, '', {
      duration: 1000,
    });
  }

  buyProducts() {
    this.productService.buyCartProducts();
    this.showBoughtSnackbar();
    this.ngOnInit();
  }

  removeFromCart(index: number) {
    this.productService.removeFromCart(index);
    this.ngOnInit();
  }
}
