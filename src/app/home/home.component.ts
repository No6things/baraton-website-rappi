import { Component, OnInit } from '@angular/core';
import { unionBy } from 'lodash';
import { finalize, filter } from 'rxjs/operators';


import { ProductService } from '../product/product.service';
import { CategoryService } from '../category/category.service';
import { Product } from '../product/product.model';
import { Category } from '../category/category.model';
import { ListFilter } from '../product/filter.interface';

@Component({
  selector: 'bar-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ProductService, CategoryService]
})

export class HomeComponent implements OnInit {

  isLoading: boolean;
  products: Product[];
  categories: Category[];
  currentCategory: Category;
  categoriesOpened: any[];
  productFilters: ListFilter[];
  breadcrumb: any;

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.categoriesOpened = [];
    this.products = [];
    this.productFilters = [{
      type: 'name',
      value: '',
    }, {
      type: 'quantity',
      value: 0,
    }, {
      type: 'minPrice',
      value: 0,
    }, {
      type: 'maxPrice',
      value: 0,
    }, {
      type: 'available',
      value: true,
    }];
    this.getCategoryList();
  }

  getCategoryList() {
    this.isLoading = true;
    this.categoryService.getCategories()
    .pipe(finalize(() => { this.isLoading = false; }))
    .subscribe(data => {
      this.categories = data;
    });
  }

  searchByName(productName: string) {
    productName = productName.trim().toLowerCase();
    const filters = [{
      type: 'name',
      value: productName
    }];

    this.searchByFilters(filters);
  }

  searchByFilters(filters: ListFilter[]) {
    this.productFilters = unionBy(filters, this.productFilters, 'type');
  }

  openSubCategories(event: Category) {
    this.categories = event.sublevels;
    this.productService.getProductsByCategory(event.id)
    .subscribe(data => {
      this.products = data;
    });
    this.updateBreadcrumb(event);
  }

  resetCategories() {
    this.ngOnInit();
  }

  updateBreadcrumb(category: any) {
    if (this.categoriesOpened.some(this.existingCategory.bind(this, category))) {
      this.categoriesOpened.pop();
    } else {
      this.categoriesOpened.push(category);
    }
  }

  existingCategory(category: any, element: any) {
    return element.name === category.name;
  }

}
