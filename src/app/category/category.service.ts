import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable, ObservableInput} from 'rxjs/Observable';
import { map, filter, first, catchError } from 'rxjs/operators';

import 'rxjs/add/observable/of';


import { Category } from './category.model';
import { of } from 'rxjs/observable/of';

@Injectable()
export class CategoryService {
  constructor(
    private http: Http
  ) { }

  public getCategories(): Observable<Category[]> {
    return this.http.get('assets/categories.json')
          .pipe(
            map((res: Response) => res.json()),
            map(body => body),
            catchError(() => of('Error, could not load categories'))
          );
  }

  public getCategory(id:number): Observable<Category[]> {
    return this.http.get('assets/categories.json')
          .pipe(
            map((res: Response) => res.json()),
            map(cat => cat.filter((el: Category) => el.id === id)),
            catchError((error) => of(`Error, could not load category ${error}`))
          );
  }

}
