import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Category } from '../../category.model';

@Component({
  selector: 'bar-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.scss'],
  providers: []
})
export class CategoryCardComponent {

  constructor( ) { }

  @Input()
  categoryDetails: Category;

  @Output()
  openSubCategories: EventEmitter<Category> = new EventEmitter<Category>();


  openCategory() {
    this.openSubCategories.emit(this.categoryDetails);
  }
}
