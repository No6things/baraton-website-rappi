import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../material.module';
import { CategoryCardComponent } from './components/card/category-card.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule
  ],
  declarations: [
    CategoryCardComponent
  ],
  exports: [
    CategoryCardComponent
  ]
})
export class CategoryModule { }
