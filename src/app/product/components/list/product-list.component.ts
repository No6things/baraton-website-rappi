import { Component, ViewChild, Input, OnChanges, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Product } from '../../../product/product.model';
import { ListFilter } from '../../filter.interface';
import { ProductService } from '../../product.service';
import {MatTableDataSource, MatSnackBar, MatPaginator, MatSort} from '@angular/material';


@Component({
  selector: 'bar-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  providers: [ProductService]
})
export class ProductListComponent implements OnChanges, AfterViewInit {

  @Input() elements: Product[];
  @Input() search: ListFilter[];

  filter: ListFilter;
  displayedColumns = ['name', 'price', 'quantity', 'available'];
  dataSource = new MatTableDataSource<Product>();
  paginatorLength = 10;

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public snackBar: MatSnackBar,
    private productService: ProductService
  ) {
    this.dataSource.filterPredicate = this.productFilters;
    this.dataSource.sortingDataAccessor = this.sortAccessor;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnChanges() {
    const filters =  {
      queries: this.search
    };

    if (this.elements && this.elements.length) {
      this.dataSource.data = this.elements;
    }

    if (this.elements && this.search !== undefined) {
      this.dataSource.filter = JSON.stringify(filters);
    }
  }

  addToCart(product: Product) {
    this.showStoredSnackbar();
    this.productService.addCartProduct(product);
  }

  productFilters(data: Product, filters: any) {
    filters = JSON.parse(filters);
    let result: any = true;

    filters.queries.map((filter: ListFilter) => {
      if (filter.type === 'name') {
        result = result && data.name.indexOf(filter.value) !== -1;
      } else if (filter.type === 'quantity') {
        result = result && data.quantity >= filter.value;
      } else if (filter.type === 'available') {
        result = result && data.available === filter.value;
      } else if (filter.type === 'minPrice') {
        const parsedPrice = parseInt(data.price.substring(1), 10);
        result = result && parsedPrice >= filter.value;
      } else if (filter.type === 'maxPrice') {
        const parsedPrice = parseInt(data.price.substring(1), 10);
        result = result && (parsedPrice <= filter.value || filter.value === 0);
      }
    });

    return result;
  }

  showStoredSnackbar() {
    const message = 'Added to Cart';
    this.snackBar.open(message, '', {
      duration: 1000,
    });
  }

  sortAccessor(data: Product, property: string)  {
    switch (property) {
      case 'available': return +data.available;
      case 'quantity': return data.quantity;
      case 'price': return parseInt(data.price.substring(1), 10);
      case 'name': return data.name;
      default: return '';
    }
  }

}
