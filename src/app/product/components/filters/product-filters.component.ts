import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ListFilter } from '../../filter.interface';

@Component({
  selector: 'bar-product-filters',
  templateUrl: './product-filters.component.html',
  styleUrls: ['./product-filters.component.scss']
})
export class ProductFiltersComponent implements OnInit {

  constructor() { }

  productFilters: any;
  @Output()
  search: EventEmitter<ListFilter[]> = new EventEmitter<ListFilter[]>();

  ngOnInit() {
    this.productFilters = {
      quantity: 0,
      minPrice: 0,
      maxPrice: 0,
      available: true
    };
  }

  searchProducts() {
    let queries: ListFilter[];

    queries = Object.keys(this.productFilters)
      .map(this.buildFilters.bind(this, this.productFilters));
    this.search.emit(queries);
  }

  buildFilters(filters: any, current: string) {
    const filter: ListFilter = {
      type: current,
      value: filters[current]
    };
    return filter;
  }

}
