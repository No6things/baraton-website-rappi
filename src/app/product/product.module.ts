import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../material.module';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { ProductListComponent } from './components/list/product-list.component';
import { ProductFiltersComponent } from './components/filters/product-filters.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    CoreModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule
  ],
  declarations: [
    ProductListComponent,
    ProductFiltersComponent
  ],
  exports: [
    ProductListComponent,
    ProductFiltersComponent
  ]
})
export class ProductModule { }
