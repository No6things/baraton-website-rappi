import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable, ObservableInput} from 'rxjs/Observable';
import { map, filter, catchError } from 'rxjs/operators';

import 'rxjs/add/observable/of';


import { Product } from './product.model';
import { of } from 'rxjs/observable/of';



@Injectable()
export class ProductService {
  constructor(
    private http: Http
  ) { }

  public getProducts(): Observable<Product[]> {
    return this.http.get('assets/products.json')
          .pipe(
            map((res: Response) => res.json()),
            map(body => body),
            catchError(() => of('Error, could not load products'))
          );
  }

  public getProductsByCategory(categoryId: number): Observable<Product[]> {
    return this.http.get('assets/products.json')
          .pipe(
            map((res: Response) => res.json()),
            map((arr: any) => {
              return arr.filter((el: any) => el.sublevel_id === categoryId);
            }),
            catchError(() => of('Error, could not load products'))
          );
  }

  public getCartProducts(): Observable<Product[]> {
    const products = JSON.parse(localStorage.getItem('cartProducts'));
    return Observable.of(products);
  }

  public addCartProduct(product: Product) {
    const cart = JSON.parse(localStorage.getItem('cartProducts')) || [];
    cart.push(product);
    localStorage.setItem('cartProducts', JSON.stringify(cart));
  }

  public buyCartProducts() {
    localStorage.setItem('cartProducts', '[]');
  }

  public removeFromCart(index: number) {
    const cart = JSON.parse(localStorage.getItem('cartProducts'));
    cart.splice(index, 1);
    localStorage.setItem('cartProducts', JSON.stringify(cart));
  }
}
