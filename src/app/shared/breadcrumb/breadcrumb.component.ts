import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from '../../category/category.model';

@Component({
  selector: 'bar-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  @Input() sections: any[];
  @Output()
  openSubCategories: EventEmitter<Category[]> = new EventEmitter<Category[]>();
  @Output()
  reset: EventEmitter<any> = new EventEmitter<any>();


  constructor() { }

  ngOnInit() { }

  resetCategories() {
    this.reset.emit();
  }

  openCategory(section: any) {
    this.openSubCategories.emit(section);
  }

}
