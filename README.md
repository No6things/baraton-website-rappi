# baraton-website-rappi

Repositorio dedicado a solucionar la primera prueba de Rappi [FE].

## Requerimientos

Tiendas “El baratón” necesita un e-commerce para expandir sus servicios, para eso don Pepe (propietario de la tienda) ha provisto de los siguientes requerimientos: 

La tienda debe mostrar categorías las cuales están conformadas por subniveles, estos subniveles a su vez pueden tener más subniveles anidados, se debe hacer un menú donde aparezcan categorías y subniveles de forma anidada. Ejemplo:

Categoría licores:
- subnivel vinos:
    - subnivel vinos tintos
    - subnivel vinos blancos
 
- Responsive. 
- Los productos tienen un identificador principal y un identificador de subnivel, esto quiere decir que un producto sólo debe ser mostrado en su subnivel correspondiente. 
- Los productos deben filtrarse por: disponibilidad, rango de precios, cantidad en stock. 
- Los productos deben poder ordenarse por precio, disponibilidad y cantidad. 
- Se debe crear un carrito de compras donde los usuarios puedan agregar, editar cantidad y eliminar un producto. 
- Los productos deben permanecer en el carrito si el usuario cierra y abre la página, solo deben ser borrados si el usuario realiza la compra. 
- Un subnivel final es aquel que no tiene más subniveles, en éste caso debe aparecer una caja de texto que permita realizar búsquedas de productos por nombre en dichos subniveles. 
- Los datos de la tienda están en dos archivos: categories.json y products.json. Se puede modificar los datos sin cambiar la estructura de los archivos

## Descripción

Este proyecto fue generado con [ngX-Rocket](https://github.com/ngx-rocket/generator-ngx-rocket/)
version 3.1.1

## Instrucciones

1. Ir a la carpeta del proyecto e instalar todas las dependencias:
 ```
 npm install
 ```

2. Levantar el servidor de desarrollo, y visitar `localhost:4200` en el navegador:
 ```
 npm start
 ```
 
## Estructura del proyecto

```
dist/                        versión de producción de la aplicación
docs/                        documentación del proyecto y guías de estilo
e2e/                         pruebas end-to-end
src/                         código fuente del proyecto
|- app/                      componentes app
|  |- core/                  modulo core (servicios singleton y componentes single-use)
|  |- shared/                modulo shared (componentes compartidos, directivas y pipes)
|  |- app.component.*        component principal app (shell)
|  |- app.module.ts          definición de módulo principal app
|  |- app-routing.module.ts  rutas de app
|  +- ...                    modulos y componenttes adicionales
|- assets/                   assets de app (imagenes, fuentes, sonidos...)
|- environments/             valores para los diversos ambientes de desarrollo
|- theme/                    variables scss globales de app y temas
|- translations/             archivos de traducciones
|- index.html                punto de entrada html
|- main.scss                 punto de entrada de estilos globales
|- main.ts                   punto de entrada de app
|- polyfills.ts              polyfills necesarios por Angular
+- test.ts                   punto de entrada de las pruebas unitarias
reports/                     reportes de pruebas y cobertura
proxy.conf.js                configuración del proxy backend
```

## Tareas principales

Las tareas automatizadas estan basadas en [scripts NPM](https://docs.npmjs.com/misc/scripts).

Tareas                            | Descripción
--------------------------------|--------------------------------------------------------------------------------------
`npm start`                     | Levanta el servidor de desarrollo en `http://localhost:4200/`
`npm run lint`                  | Analiza el código
`npm run docs`                  | Muestra la documentación del código


## Servidor de desarrollo

Ejecute `npm start` para levantar el servidor de desarrollo. Navegue hacia `http://localhost:4200/`. La aplicación recargará automaticamente si modifica
alguno de los archivos fuentes.
No deberia ejecutar `ng serve` directamente, ya que no hace uso de las configuraciones para el proxy backend por defecto.

## Código scaffolding

Ejecute `npm run generate -- component <nombre>` para generar un nuevo componente. Tambien puede hacer uso de
`npm run generate -- directive|pipe|service|class|module`.

Si ha instalado [angular-cli](https://github.com/angular/angular-cli) globalmente con `npm install -g @angular/cli`,
tambien puede ejecutar `ng generate` directamente.

## Herramientas adicionales

Las tareas estan basadas mayormente en la herramienta `angular-cli`. Use `ng help` para obtener ayuda o visite
[Angular-CLI README](https://github.com/angular/angular-cli).

## Qué hay en la caja?

La plantilla de la aplicación esta basada en [HTML5](http://whatwg.org/html), [TypeScript](http://www.typescriptlang.org) y
[Sass](http://sass-lang.com). Los archivos de traducción usan el formato [JSON](http://www.json.org) usado comúnmente.

#### Herramientas

Los procesos de desarrollo, construcción y calidad estan basados en [angular-cli](https://github.com/angular/angular-cli) y
[scripts NPM](https://docs.npmjs.com/misc/scripts), lo que incluye:

- Proceso de bundling y construcción optimizado con [Webpack](https://webpack.github.io)
- [Servidor de desarrollo](https://webpack.github.io/docs/webpack-dev-server.html) con backend proxy y live reload
- CSS multi-navegador con [autoprefixer](https://github.com/postcss/autoprefixer) y
  [browserslist](https://github.com/ai/browserslist)
- Revision de assets para [mejor manejo de cache](https://webpack.github.io/docs/long-term-caching.html)
- Pruebas unitarias usando [Jasmine](http://jasmine.github.io) y [Karma](https://karma-runner.github.io)
- Pruebas end-to-end usando [Protractor](https://github.com/angular/protractor)
- Analisis de código estático usando: [TSLint](https://github.com/palantir/tslint), [Codelyzer](https://github.com/mgechev/codelyzer),
  [Stylelint](http://stylelint.io) y [HTMLHint](http://htmlhint.com/)
- Servidor de documentación usando [Hads](https://github.com/sinedied/hads)

#### Bibliotecas

- [Angular](https://angular.io)
- [Angular Material](https://material.angular.io)
- [Angular Flex Layout](https://github.com/angular/flex-layout)
- [Material Icons](https://material.io/icons/)
- [RxJS](http://reactivex.io/rxjs)
- [ngx-translate](https://github.com/ngx-translate/core)
- [Lodash](https://lodash.com)

#### Guías de estilo

- [Angular](docs/coding-guides/angular.md)
- [TypeScript](docs/coding-guides/typescript.md)
- [Sass](docs/coding-guides/sass.md)
- [HTML](docs/coding-guides/html.md)
- [Pruebas unitarias](docs/coding-guides/unit-tests.md)
- [Pruebas e2e](docs/coding-guides/e2e-tests.md)

#### Otra documentación

- [Guía I18n](docs/i18n.md)
- [Trabajando detras de un proxy empresarial](docs/corporate-proxy.md)
- [Actualizando herramientas y dependencias](docs/updating.md)
- [Usando proxy empresarial para desarrollo](docs/backend-proxy.md)
- [Enrutamiento](docs/routing.md)
